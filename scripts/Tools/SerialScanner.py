#!/usr/bin/env python

import serial
import sys
import glob  # for listing serial ports
import time


def getSerialPorts():
    """Lists serial ports
    From http://stackoverflow.com/questions/12090503/listing-available-com-ports-with-python

    :raises EnvironmentError:
        On unsupported or unknown platforms
    :returns:
        A list of available serial ports
    """
    if sys.platform.startswith('win'):
        ports = ['COM' + str(i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this is to exclude your current terminal "/dev/tty"
        print("Looking for Linux Ports")
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


def findSmartMotor(openablePorts):
    if(openablePorts != []):
        serialDeviceDict = {}
        for port in openablePorts:
            try:
                s = serial.Serial(port, baudrate=38400, timeout=2)
                print("Testing Port:" + port)

                s.write("PRINT(j)\r".encode("utf-8"))
                data = s.read(16)
                string = data.decode('utf-8')

                if '6678' in string:
                    print('Steering Motor found on: ' + port)
                    serialDeviceDict['steeringSmartMotor'] = port
                elif '5532' in string:
                    print('Shifting Motor found on: ' + port)
                    serialDeviceDict['shiftingSmartMotor'] = port
                s.close()
            except Exception as Ex:
                print(Ex)
                if(s.is_open): s.close()
                print("Failed to Open: " + port)

        return serialDeviceDict
    else:
        return {}


def findMyRIO(openablePorts):
    if(openablePorts != []):
        serialDeviceDict = {}
        my_RIO_found = False
        for port in openablePorts:
            try:
                # Open Port, would have to adjust baudrates if the myRIO needs to be discovered also.
                s = serial.Serial(port, baudrate=115200, timeout=1)
                print("Testing Port:" + port)

                start_time = time.time()
                current_time = start_time
                # Check for data for 2 seconds then move to next port
                while current_time < start_time + 2.0 and not my_RIO_found:
                    current_time = time.time()
                    incomingBytes = s.read(1)
                    # Use ord to get the decimal values of the bytes
                    if(len(incomingBytes) > 0):
                        if(ord(incomingBytes) == 157):
                            incomingBytes = s.read(8)
                            if(ord(incomingBytes[7]) == 147):
                                print("MyRIO found on: " + port)
                                serialDeviceDict["myRIO"] = port
                                my_RIO_found = True
                s.close()
                if(my_RIO_found): break
            except Exception as Ex:
                print(Ex)
                if(s.is_open): s.close()
                print("Failed to Open: " + port)

        return serialDeviceDict
    else:
        return {}


if __name__ == '__main__':
    try:
        ports = getSerialPorts()
        ports.remove('/dev/ttyACM0') # This is an arduino
        print("Searching for Smart Motors")
        smart_motors = findSmartMotor(openablePorts=ports)
        print("Searching for MyRIO")
        my_RIO = findMyRIO(openablePorts=ports)

        serial_devices = {}
        serial_devices.update(smart_motors)
        serial_devices.update(my_RIO)
        print(serial_devices)
    except Exception as Ex:
        print(Ex)
