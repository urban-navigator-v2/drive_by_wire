#! /usr/bin/env python

import serial
from time import sleep
import sys

# Arduino will reset everytime a serial connection is established.

s = serial.Serial('/dev/ttyACM0', 9600)

sleep(5)

# 128 == On
# 255 == Off

if(len(sys.argv) > 1):
    if(sys.argv[1] == 'on' or sys.argv[1] == 'ON' or sys.argv[1] == 'On'):
        s.write(bytearray([86, 128, 107]))
        print('Motor Power On')
    elif(sys.argv[1] == 'off' or sys.argv[1] == 'OFF' or sys.argv[1] == 'Off'):
        s.write(bytearray([86, 255, 107]))
        print('Motor Power Off')
    else:
        print('Arguments not valid, use commandline arguments: "on" or "off"')
else:
    print('No Arguments given, use commandline arguments: "on" or "off"')

s.close()
