#!/usr/bin/env python

'''
Written by: Patrick Neal, neap@ufl.edu

Requirements:
	Need to have "messages" repository installed from Autoware.ai gitlab

To Do:
'''

import rospy
from autoware_msgs.msg import VehicleCmd, AccelCmd, SteerCmd, BrakeCmd, LampCmd
from tablet_socket_msgs.msg import mode_cmd, gear_cmd
from sensor_msgs.msg import Joy
from geometry_msgs.msg import TwistStamped

# vehicleCmdPub = rospy.Publisher('/vehicle_cmd', VehicleCmd, queue_size=10)
sendVehicleCmd = VehicleCmd()

accelCmdPub = rospy.Publisher('/accel_cmd', AccelCmd, queue_size=10)
steerCmdPub = rospy.Publisher('/steer_cmd', SteerCmd, queue_size=10)
brakeCmdPub = rospy.Publisher('/brake_cmd', BrakeCmd, queue_size=10)
lampCmdPub = rospy.Publisher('/lamp_cmd', LampCmd, queue_size=10)
modeCmdPub = rospy.Publisher('/mode_cmd', mode_cmd, queue_size=10)
gearCmdPub = rospy.Publisher('/gear_cmd', gear_cmd, queue_size=10)
twistPub = rospy.Publisher('/twist_raw', TwistStamped, queue_size=10)

sendAccelCmd = AccelCmd()
sendSteerCmd = SteerCmd()
sendBrakeCmd = BrakeCmd()
sendLampCmd = LampCmd()
sendModeCmd = mode_cmd()
sendGearCmd = gear_cmd()
sendTwist = TwistStamped()

autoMode = 0
leftCounter = 0
rightCounter = 0


def joyCallback(data):
	"""
	Converts Joy messages from Xbox 360 Controller to VehicleCmd
	Available data from Xbox 360 Controller using Joy
	--------------------------------------------------
	Axes:
		axes[0]: Left Thumbstick-Horizontal,Range: [1,-1], 0 Centered, 1 Left
		axes[1]: Left Thumbstick-Vertical,	Range: [1,-1], 0 Centered, 1 Up
		axes[2]: Left Trigger,				Range: [1,-1], 1 when unpressed
		axes[3]: Right Thumbstick-Horizontal,Range: [1,-1], 0 Centered, 1 Left
		axes[4]: Right Thumbstick-Vertical, Range: [1,-1], 0 Centered, 1 Up
		axes[5]: Right Trigger,				Range: [1,-1], 1 when unpressed
		axes[6]: Horizontal D-Pad			Vales: [1], [0], [-1], 1 pressed left, -1 pressed right, 0 not pressed
		axes[7]: Verical D-Pad				Vales: [1], [0], [-1], 1 pressed up, -1 pressed down, 0 not pressed
	Buttons:
		buttons[0]: "A",					Values: [0], [1], 1 when pressed
		buttons[1]: "B",					Values: [0], [1], 1 when pressed
		buttons[2]: "X",					Values: [0], [1], 1 when pressed
		buttons[3]: "Y",					Values: [0], [1], 1 when pressed
		buttons[4]: Left Bumper,			Values: [0], [1], 1 when pressed
		buttons[5]: Right Bumper,			Values: [0], [1], 1 when pressed
		buttons[6]: "Back" Button,			Values: [0], [1], 1 when pressed
		buttons[7]: "Start" Button,			Values: [0], [1], 1 when pressed
		buttons[8]: Xbox Button,			Values: [0], [1], 1 when pressed
		buttons[9]: Left Thumbstick Press,  Values: [0], [1], 1 when pressed
		buttons[10]: Right Thumbstick Press,Values: [0], [1], 1 when pressed
		Notes:
			Button presses exhibit a bouncing behavior, multiple message will be sent for a single button press.
			Additional buttons are also available past Button[5]

	Converting from Joy to autoware_msgs/VehicleCmd
	"""
	# TODO: Tune mappings for easier teleop driving.
	global sendVehicleCmd, leftCounter, rightCounter

	sendVehicleCmd.twist_cmd.twist.linear.x = 5*abs((data.axes[5] - 1))
	sendVehicleCmd.twist_cmd.twist.angular.z = 0.5*data.axes[3]  # units of rad/s

	# Left and Right turn signals. Bumper presses are mutually exclusive
	if(data.buttons[4] == 1):
		leftCounter += 1
	if(data.buttons[5] == 1):
		rightCounter += 1

	if(leftCounter >= 3):
		leftCounter = 0
		sendVehicleCmd.lamp_cmd.l = not sendVehicleCmd.lamp_cmd.l

	if(rightCounter >= 3):
		rightCounter = 0
		sendVehicleCmd.lamp_cmd.r = not sendVehicleCmd.lamp_cmd.r

	# Gear selection. Should only output 1 gear recommendation
	# 0 drive, 1 reverse, 2 park, 3 low, 4 neutral
	# priotity: park, drive, reverse, neutral
	if(data.axes[6] == 1):
		sendVehicleCmd.gear = 2
	elif(data.axes[7] == 1):
		sendVehicleCmd.gear = 0
	elif(data.axes[7] == -1):
		sendVehicleCmd.gear = 1
	elif(data.axes[6] == -1):
		sendVehicleCmd.gear = 4

	# Start button will engage auto, while the back button will release it.
	global autoMode
	if(data.buttons[7] == 1 and sendVehicleCmd.mode != 1):
		sendVehicleCmd.mode = 1
	elif(data.buttons[6] == 1 and sendVehicleCmd.mode == 1):
		sendVehicleCmd.mode = 0

	sendLampCmd = sendVehicleCmd.lamp_cmd
	sendGearCmd.gear = sendVehicleCmd.gear
	sendModeCmd.mode = sendVehicleCmd.mode
	sendTwist = sendVehicleCmd.twist_cmd

	lampCmdPub.publish(sendLampCmd)
	gearCmdPub.publish(sendGearCmd)
	modeCmdPub.publish(sendModeCmd)
	twistPub.publish(sendTwist)

	# vehicleCmdPub.publish(sendVehicleCmd)


def Node():
	"""
	This ROS node is responsible for converting Xbox 360 controller inputs
	to autoware_msgs/VehicleCmd messages. Where the data being converted is
	trigger values to linear velocity and yaw rate of a vehicle.
	"""

	rospy.Subscriber("joy", Joy, joyCallback)

	# pub_vehicle_cmd = rospy.get_param('teleop_tester/pub_vehicle_cmd', 'False')

	rospy.init_node('teleop_tester', anonymous=True)

	rospy.spin()


if __name__ == '__main__':
	try:
		Node()
	except rospy.ROSInterruptException:
		print("Program Failed")
		pass
