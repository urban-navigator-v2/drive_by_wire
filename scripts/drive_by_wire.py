#!/usr/bin/env python
'''
This sotware is used as the interface to the hardware that enables
drive by wire functionality of the Urban NaviGator.
The hardware is:
    - A SmartMotor responsible for turning the steering column
    - A Animatics SmartMotor responsible for shifting gears
    - A NI MyRIO 9000 that is responsible for controlling:
        * Throttle effort
        * Brake Effort
        * Triggering Brake switch
        * Kill Switches
    - An Arduino located in the rear of the vehicle responsible for
    toggling power to the steering SmartMotor

Create by: Patrick Neal, neap@ufl.edu
Autoware Compatibility: 1.12
'''


# import sys
import rospy
# import serial
# from time import sleep
from math import asin, atan2, tan
from std_msgs.msg import Bool
# from geometry_msgs.msg import Odometry
from nav_msgs.msg import Odometry
from geometry_msgs.msg import TwistStamped
from SerialDeviceClass import MyRIO, SteeringSmartMotor, ShiftingSmartMotor, enforceBounds
from autoware_msgs.msg import VehicleCmd, VehicleStatus


rosDebug = False
serialDebug = False

desiredGearCmd = 2
leftSignal = False
rightSignal = False
autoMode = False

steeringCmd = 0  # Final value should have units encoder counts
accelEffort = 0	  # Final value should be in units 0-100% effort
brakingEffort = 0  # Final value should be in units 0-100% effort

vehicleSpeed = 0.0  # Forward speed of the vehicle in (m/s)
k_brake = 35
k_throttle = 30

lr = 2.71526 / 2  # meters
lf = 2.71526 / 2  # meters

debugCount = 0


def vehicleCmdCallback(data):
    mapTwistToEfforts(data.twist_cmd.twist)

    global desiredGearCmd, leftSignal, rightSignal, autoMode
    if(data.gear_cmd.gear == 0):
        desiredGearCmd = 5  # Park
    else:
        desiredGearCmd = data.gear_cmd.gear

    leftSignal = True if data.lamp_cmd.l else False
    rightSignal = True if data.lamp_cmd.r else False
    autoMode = True if data.mode else False


def mapTwistToEfforts(twist):
    """
    Takes a twist and maps it to corresponding: throttle and braking efforts
    (0-100%), steering angle in encoder counts. Requires vehicle velocity feedback
    Only handles moving forward, no reverse velocities.
    """
    global steeringCmd, accelEffort, brakingEffort, vehicleSpeed, debugCount

    # print("Vx: {:2.3f}, Vy: {:2.3f}, Vz: {:2.3f}, wx: {:2.3f}, wy: {:2.3f}, wz: {:2.3f}".format(twist.linear.x, twist.linear.y, twist.linear.z, twist.angular.x, twist.angular.y, twist.angular.z))

    speedDelta = vehicleSpeed - twist.linear.x
    if(abs(speedDelta) < 0.25 and twist.linear.x < 0.1):
        # Stop the vehicle from coasting when no velocity is commanded
        brakingEffort = 35
        accelEffort = 0
    elif(speedDelta > 0 and abs(speedDelta) >= 0.25):
        # We need to slow down, apply brake effort
        brakingEffort = enforceBounds(k_brake * abs(speedDelta), upper=50)
        accelEffort = 0
    elif(speedDelta < 0 and abs(speedDelta) >= 0.25):
        # We need to speed up, apply throttle effort
        accelEffort = enforceBounds(k_throttle * abs(speedDelta), upper=50)
        brakingEffort = 0
    else:
        brakingEffort = 0
        accelEffort = 0

    """
    if(debugCount > 10):
        print("Speed: {:2.3f} Accel: {:2.3f} Brake: {:2.3f}".format(vehicleSpeed, accelEffort, brakingEffort))
        debugCount = 0
    else:
        debugCount = debugCount + 1
    """

    if(twist.linear.x < 0.5):
        steeringCmd = 0
    else:
        tempVal = twist.angular.z * lr / twist.linear.x
        if(tempVal <= 1 and tempVal >= -1):
            beta = asin(tempVal)
            steeringCmd = atan2((lf + lr) * tan(beta), lr)  # Tire angle, units of rad
        else:
            print('Unfeasible twist command!')


def odometryCallback(data):
    global vehicleSpeed
    vehicleSpeed = data.twist.linear.x


def steeringPwrStateCallback(data):
    pass


def dbwNode():

    vehStatusPub = rospy.Publisher("vehicle_status", VehicleStatus, queue_size=10)

    rospy.Subscriber("vehicle_cmd", VehicleCmd, vehicleCmdCallback)
    rospy.Subscriber("/current_velocity", TwistStamped, odometryCallback)
    rospy.Subscriber("steering_pwr_state", Bool, steeringPwrStateCallback)

    # Initialize ROS Node
    rospy.init_node('drive_by_wire', anonymous=True)

    # -------------- Connect to serial devices -----------------

    try:
        myRIO = MyRIO(rospy.get_param('drive_by_wire/myRIO_port', default='/dev/ttyUSB7'),
                      rospy.get_param('drive_by_wire/myRIO_baudrate', default=115200))
        print("myRIO: Port opened sucessfully")
    except Exception as ex:
        print(ex)
        print('myRIO: Failed to Open')
        if not rosDebug:
            return

    try:
        steeringSmartMotor = SteeringSmartMotor(rospy.get_param('drive_by_wire/steeringMotor_port', default='/dev/ttuUSB10'),
                                                rospy.get_param('drive_by_wire/steeringMotor_baudrate', default=38400))
        print("steeringSmartMotor: Port opened sucessfully")
    except Exception as ex:
        print(ex)
        print('steeringSmartMotor: Failed to Open')
        if not rosDebug:
            myRIO.close()
            return

    try:
        shiftingSmartMotor = ShiftingSmartMotor(rospy.get_param('drive_by_wire/shiftingMotor_port', default='/dev/ttyUSB11'),
                                                rospy.get_param('drive_by_wire/shiftingMotor_baudrate', default=38400))
        print("shiftingSmartMotor: Port opened sucessfully")
    except Exception as ex:
        print(ex)
        print('shiftingSmartMotor: Failed to Open')
        if not rosDebug:
            myRIO.close()
            steeringSmartMotor.close()
            return

    # ----------- Make SmartMotors go through homing routine ------------

    if (not rosDebug):
        # These will block for about 10 seconds if Smart Motor never returns "READY"
        if (steeringSmartMotor.initializeMotor()):
            print('Steering Motor Ready')
        else:
            print('Steering Motor failed to initialize')

        if(shiftingSmartMotor.initializeMotor()):
            print('Shifting Motor Ready')
        else:
            print('Shifting Motor failed to initialize')

    gearConvDict = {0: "NONE", 1: "Drive", 2: "Reverse", 5: "Park", 3: "Regen", 4: "Neutral"} # 1.13
    # gearConvDict = {0: "NONE", 1: "Park", 2: "Reverse", 4: "Drive", 5: "Regen", 3: "Neutral"} # 1.15+
    vehicleStatusMsg = VehicleStatus()
    lastTime = 0
    while not (rospy.is_shutdown()):
        currentTime = rospy.Time.now().to_nsec()
        # Calculate a time and then send messages at 25ms intervals
        if((currentTime / 1000.0 - lastTime / 1000.0) > 25000):
            lastTime = currentTime
            if(not rosDebug):
                # Determine shifting state first
                #  print('Brake: {}, Accel: {}, Steer: {}'.format(brakingEffort, accelEffort, steeringCmd))
                if(shiftingSmartMotor.currentGear != shiftingSmartMotor.desiredGear and myRIO.State == "AUTONOMOUS"):
                    if(vehicleSpeed < 0.1):
                        # Lock effort and steering commands and perform shift
                        shiftingSmartMotor.inShiftWaiting = False
                        myRIO.setState(autoReq=autoMode, Brake=35, Throttle=0)
                        myRIO.sendMessage()
                        shiftingSmartMotor.shiftGear(desiredGear=shiftingSmartMotor.desiredGear)
                    elif(not shiftingSmartMotor.inShiftWaiting):
                        shiftingSmartMotor.inShiftWaiting = True
                        print('I am waiting for vehicle to stop')

                if(not shiftingSmartMotor.isShifting):
                    myRIO.setState(autoReq=autoMode, Brake=brakingEffort, Throttle=accelEffort, leftBlinker=leftSignal, rightBlinker=rightSignal)
                    steeringSmartMotor.setDesiredTireAngle(steeringCmd)
                    myRIO.sendMessage()
                    steeringSmartMotor.sendMessage()
                    shiftingSmartMotor.desiredGear = gearConvDict[desiredGearCmd]

                else:
                    print("I am waiting for shift to finish")

        # vehicleStatusMsg.gearshift = 1 if shiftingSmartMotor.isShifting else 0

        if(myRIO.State == "MANUAL"):
            vehicleStatusMsg.drivemode = vehicleStatusMsg.MODE_MANUAL
        elif(myRIO.State == "AUTONOMOUS"):
            vehicleStatusMsg.drivemode = vehicleStatusMsg.MODE_AUTO
            vehicleStatusMsg.steeringmode = vehicleStatusMsg.MODE_AUTO

        vehicleStatusMsg.speed = vehicleSpeed / 1000.0  # km/s
        vehicleStatusMsg.drivepedal = myRIO.currentThrottle
        vehicleStatusMsg.brakepedal = myRIO.currentBrake
        vehicleStatusMsg.angle = steeringSmartMotor.currentSteeringAngle  # rad, tire angle !! This needs to be update with tire angle not steering angle.
        if(myRIO.leftBlinker and myRIO.rightBlinker):
            vehicleStatusMsg.lamp = vehicleStatusMsg.LAMP_HAZARD
        elif(myRIO.leftBlinker):
            vehicleStatusMsg.lamp = vehicleStatusMsg.LAMP_LEFT
        elif(myRIO.rightBlinker):
            vehicleStatusMsg.lamp = vehicleStatusMsg.LAMP_RIGHT
        else:
            vehicleStatusMsg.lamp = 0

        vehStatusPub.publish(vehicleStatusMsg)

    if not rosDebug:
        print("Closing Serial Connections")
        if myRIO.is_open: myRIO.close()
        if steeringSmartMotor.is_open: steeringSmartMotor.close()
        if shiftingSmartMotor.is_open: shiftingSmartMotor.close()


if __name__ == '__main__':
    try:
        dbwNode()
    except rospy.ROSInterruptException:
        print("Progam Finished")
    except Exception as Ex:
        print("Program faulted")
        print(Ex)
