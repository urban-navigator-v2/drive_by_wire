#!/usr/bin/env python

import serial
from time import sleep
import threading
from math import pi, floor


def enforceBounds(Number, lower=0, upper=99):
    """
    Takes a number and returns an integer between [0, 99]
    """
    if(Number > upper):
        Number = upper
    elif(Number < lower):
        Number = lower

    return int(Number)


class serialDevice(serial.Serial):

    def sendMessage(self):
        self.write(self.createMessage())

    def createMessage(self):
        raise NotImplementedError

    def parseReceivedMessage(self):
        raise NotImplementedError

    def _receiveThread(self):
        raise NotImplementedError

    def receiveThread(self):
        self.th = threading.Thread(target=self._receiveThread)
        self.th.daemon = True
        self.th.start()


"""
=================================================================================================================
"""


class MyRIO(serialDevice):
    """
    Serial communications is achieved through byte data only when using Python 3, strings otherwise.
    """
    def __init__(self, port, baudrate):
        super(MyRIO, self).__init__(port, baudrate)

        # Maybe a dictionary
        # State of the program
        self.Throttle = 0
        self.Brake = 0
        self.leftBlinker = 0
        self.rightBlinker = 0
        self.pauseReq = 0
        self.autoReq = 0

        # State of the myRIO
        self.currentThrottle = 0
        self.currentBrake = 0
        self.currentLeftBlinker = False
        self.currentRightBlinker = False
        self.paused = False
        self.StateDict = {64: "MANUAL", 128: "AUTONOMOUS", 255: "ESTOPPED", 32: "INACTIVE"}
        self.State = "INACTIVE"

        self.receiveThread()

    def setState(self, autoReq=False, Brake=0, Throttle=0, leftBlinker=False, rightBlinker=False, pauseReq=False):
        self.autoReq = int(128) if autoReq else int(64)
        self.Brake = enforceBounds(Brake)
        self.Throttle = enforceBounds(Throttle)
        self.leftBlinker = int(128) if leftBlinker else int(64)
        self.rightBlinker = int(128) if rightBlinker else int(64)
        self.pauseReq = int(128) if pauseReq else int(64)

    def createMessage(self):
        crc = self.calculateCRCRemainder()
        return bytearray([137, self.autoReq, self.pauseReq, self.leftBlinker, self.rightBlinker, self.Brake, self.Throttle, crc, 127])

    def calculateCRCRemainder(self):
        crcString = str(self.autoReq) + str(self.pauseReq) + str(self.leftBlinker) + str(self.rightBlinker) + str(self.Brake) + str(self.Throttle)
        return int(crcString) % 256

    def parseReceivedMessage(self, receivedBytes):
        # receivedBytes is a list of integers
        self.State = self.StateDict[receivedBytes[0]]
        self.paused = True if receivedBytes[1] == 128 else False
        self.currentLeftBlinker = True if receivedBytes[2] == 128 else False
        self.currentRightBlinker = True if receivedBytes[3] == 128 else False
        self.currentBrake = receivedBytes[4]
        self.currentThrottle = receivedBytes[5]

    def _receiveThread(self):
        # TO DO: Add crc checking after myRIO code is updated.
        try:
            while self.is_open:
                if(self.in_waiting):
                    incomingBytes = self.read(1)
                    # Use ord to get the decimal values of the bytes
                    incomingBytes = [ord(i) for i in incomingBytes]
                    if(incomingBytes[0] == 157):
                        incomingBytes = self.read(8)
                        if(ord(incomingBytes[7]) == 147):
                            incomingBytes = [ord(i) for i in incomingBytes]
                            self.parseReceivedMessage(incomingBytes)
        except Exception as ex:
            print(ex)  # Most likely the main program has closed the device or ended so just move on


"""
=================================================================================================================
"""


class SteeringSmartMotor(serialDevice):
    """
    Class specifically for the Smart Motor attached to the steering column of the Urban NaviGator
    Serial communication is accomplished through ASC-II characters.

    Default buadrate: 384000

    """
    def __init__(self, port, baudrate):
        super(SteeringSmartMotor, self).__init__(port, baudrate)

        self.desiredSteeringAngle = 0  # rad
        self.currentSteeringAngle = 0  # rad
        self.desiredSteeringCmd = 0  # Has units encoder counts
        self.state = 'INACTIVE'

        self.receiveThread()

    def initializeMotor(self):
        print("Initializing the steering motor")
        self.write('RUN\r'.encode('utf-8'))
        count = 0
        # Wait for the Smart Motor to be ready, it has to home first
        while not (self.state == "READY"):
            count += 1
            if(count > 20):
                return False
            sleep(0.5)
        return True

    def setDesiredTireAngle(self, desiredTireAngle):
        " inputs units of rad of tire angle, sets SmartMotor command"
        self.desiredSteeringAngle = desiredTireAngle
        # 15.6 is the steering ratio for the toyota, 155000/1.45 rev is the smart motor encoder relation
        self.desiredSteeringCmd = enforceBounds(floor(desiredTireAngle * 15.6 * 155000 / (2 * pi * 1.45)), lower=-150000, upper=150000)  # units of encoder counts

    def createMessage(self):
        message = 'p=' + str(self.desiredSteeringCmd) + '\r'
        return message.encode('utf-8')

    def parseReceivedMessage(self, receivedDataString):
        if("c=" in receivedDataString):
            receivedDataString = receivedDataString.replace('c=', '')
            self.currentSteeringAngle = int(receivedDataString) * (1.45 * 2 * pi) / (155000 * 15.6)  # tire angle, units of rad
        else:
            if("READY" in receivedDataString):
                self.state = receivedDataString
            elif("NORMAL_SHUTDOWN" in receivedDataString):
                self.state = "SHUTDOWN"

    def _receiveThread(self):
        receivedDataString = ''
        try:
            while self.is_open:
                if(self.inWaiting):
                    receivedData = self.read()
                    if(not receivedData.decode('utf-8') == '\r'):
                        receivedDataString += receivedData.decode('utf-8')
                    else:
                        self.parseReceivedMessage(receivedDataString)
                        receivedDataString = ""
        except Exception as ex:
            print(ex)  # Most likely the main program has closed the device or ended so just move on


"""
=================================================================================================================
"""


class ShiftingSmartMotor(serialDevice):
    """
    Class specifically for the Smart Motor used to move the shifter of the Urban NaviGator
    Serial communication is accomplished through ASC-II characters. SmartMotor baudrate is 38400
    Vehicle should be in park when this code is initiated. SmartMotor might autonmatically home to park
    anyways.
    """
    def __init__(self, port, baudrate):
        super(ShiftingSmartMotor, self).__init__(port, baudrate)

        # Maybe a dictionary
        self.desiredGear = 'Park'
        self.currentGear = 'Park'
        self.isShifting = False  # Set to true if the motor is performing a gear shift
        self.inShiftWaiting = False  # Set to true if a new gear is desired but vehicle condition doesn't allow changing gears
        self.brakeState = False
        self.state = 'INACTIVE'
        self.stateDict = {'Park': 's=0\r', 'Reverse': 's=255\r', 'Neutral': 's=128\r', 'Drive': 's=1\r', 'Regen': 's=2\r'}

        self.receiveThread()  # Start thread for reading, see serialDevice Class

    def shiftGear(self, desiredGear='Park'):
        self.desiredGear = desiredGear
        self.sendMessage()

    def initializeMotor(self):
        print("Initializing the shifting motor")
        sleep(0.1)
        self.write('RUN\r'.encode('utf-8'))
        sleep(0.1)
        self.write('h=1\r'.encode('utf-8'))
        count = 0
        # Wait for the Smart Motor to be ready, it has to home first
        while not (self.state == "READY"):
            count += 1
            if(count > 50):
                return False
            sleep(0.5)

        return True

    def createMessage(self):
        return self.stateDict[self.desiredGear].encode('utf-8')

    def parseReceivedMessage(self, receivedDataString):
        if('BRAKE_PEDAL_HIGH' in receivedDataString):
            self.brakeState = False
        elif('BRAKE_PEDAL_LOW' in receivedDataString):
            self.brakeState = True
        elif('PARK' in receivedDataString):
            self.currentGear = 'Park'
            self.isShifting = False
        elif('REVERSE' in receivedDataString):
            self.currentGear = 'Reverse'
            self.isShifting = False
        elif('NEUTRAL' in receivedDataString):
            self.currentGear = 'Neutral'
            self.isShifting = False
        elif('DRIVE' in receivedDataString):
            self.currentGear = 'Drive'
            self.isShifting = False
        elif('REGEN' in receivedDataString):
            self.currentGear = 'Regen'
            self.isShifting = False
        elif('ACTUATING' in receivedDataString):
            self.isShifting = True
        elif("READY" in receivedDataString):
            self.state = receivedDataString
            self.isShifting = False
        elif("NORMAL_SHUTDOWN" in receivedDataString):
            self.state = "SHUTDOWN"

    def _receiveThread(self):
        receivedDataString = ''
        try:
            while self.is_open:
                if(self.inWaiting):
                    receivedData = self.read()
                    if(not receivedData.decode('utf-8') == '\r'):
                        receivedDataString += receivedData.decode('utf-8')
                    else:
                        self.parseReceivedMessage(receivedDataString)
                        receivedDataString = ""
        except Exception as ex:
            print(ex)  # Most likely the main program has closed the device or ended so just move on


"""
=================================================================================================================
"""


class Rear_Relay_Controller(serialDevice):
    """
    This is setup to send commands to the Arduino Uno in the back of the Urban NaviGator.
    This arduino controls relays that will power the steering motor on/off
    Serial communications is achieved through byte data only when using Python 3, strings otherwise.
    """
    def __init__(self, port, baudrate):
        super(MyRIO, self).__init__(port, baudrate)

        self.motorPowerState = True
        self.requestedMotorPowerState = True

        self.receiveThread()

    def setMotorPwrState(self, reqMotorPwrState=False):
        self.requestedMotorPowerState = reqMotorPwrState

    def createMessage(self):
        byteValue = 128 if self.requestedMotorPowerState else 255
        return bytearray([86, byteValue, 107])

    def parseReceivedMessage(self, receivedBytes):
        pass

    def _receiveThread(self):
        try:
            while self.is_open:
                if(self.in_waiting):
                    pass
        except Exception as ex:
            print(ex)


"""
=================================================================================================================
"""


def testClassNode():

    myRIO = MyRIO('/dev/ttyACM1', 115200)
    steeringSmartMotor = SteeringSmartMotor('/dev/ttyACM0', 38400)
    shiftingSmartMotor = ShiftingSmartMotor('/dev/ttyACM2', 38400)

    sleep(10)

    # This will block if Smart Motor never returns "READY"
    if (steeringSmartMotor.initializeMotor()):
        print('Steering Motor Ready')

    if(shiftingSmartMotor.initializeMotor()):
        print('Shifting Motor Ready')

    count = 0
    while(count < 100):

        if(count == 25):
            shiftingSmartMotor.shiftGear(desiredGear='Drive')
        elif(count == 75):
            shiftingSmartMotor.shiftGear(desiredGear='Reverse')

        steeringSmartMotor.setDesiredSteeringAngle(count * 200)
        steeringSmartMotor.sendMessage()

        myRIO.setState(autoReq=True, Throttle=count)
        try:
            myRIO.sendMessage()
        except Exception as ex:
            print(ex)
            pass

        # print(myRIO.currentThrottle)
        count = count + 1
        sleep(0.1)

    print("Closing Serial Connections")
    myRIO.close()
    steeringSmartMotor.close()
    shiftingSmartMotor.close()


if __name__ == '__main__':
    try:
        testClassNode()
    except Exception as ex:
        print(ex)
