# Urban NaviGator Interface
This code will be used as the bridge between the Autoware.ai autonomy and the vehicle. This is a work in progress.

```
roslaunch drive_by_wire urban_navigator_dbw.launch
```

Warning: currently configured to work with Autoware 1.13.0, will investigate creating a branch for it.

## External Requirements
* Autoware.ai "messages" repository


## ROS Topics
### Subscribes
* __autoware_msgs/VehicleCmd:__ /vehicle_cmd
* __geometry_msgs/TwistStamped:__  /current_velocity
* __Bool:__  /steering_pwr_state

### Publishes
* None

## Telop Tester Tool
View launch file to edit **joy_node** parameters
```
roslaunch drive_by_wire teleop_tester.launch
```
### Requirements
* Xbox 360 Wire Controller
* "joy" ROS package installed

### Known Issues
* left and right bumper toggling of turn signals is inconsistent due to debouncing the buttons.
* It is possible to send kinematically unfeasible twist commands